import test from 'ava';
import sinon from 'sinon';
import devisionController from './devisionController';
// const devisionController = {
//   count(number) {
//     return number;
//   },
// };

// test('should 1 be equal 1', (t) => {
//   t.is(1, 1);
// });

// test('should test devisionController count method', (t) => {
//   t.plan(2);

//   sinon.spy(devisionController, 'count');

//   devisionController.count(2);

//   t.true(devisionController.count.args[0].length === 2);
//   t.true(devisionController.count.calledWith(2));

//   // t.throws(devisionController.count(2, 5));
// });

test('isNaN true', (t) => {
  t.true(isNaN(devisionController.count('s', 5)));
});
test('should return 2', (t) => {
  t.is(devisionController.count(10, 5), 2);
});
test('should return infinity', (t) => {
  t.is(devisionController.count(10, 0), Infinity);
});

