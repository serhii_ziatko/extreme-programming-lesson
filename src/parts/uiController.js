import devisionController from './devisionController';
import multiplyController from './multiplyController';

let firstNumber;
let secondNumber;
let action;
let result;


export default {
  setFirst(arg) {
    firstNumber = arg;
  },

  setSecond(arg) {
    secondNumber = arg;
  },
  setAction(arg) {
    action = arg;
  },

  getResult() {
    if (action === '*') {
      result = multiplyController.count(firstNumber, secondNumber);
    } else if (action === ':') {
      result = devisionController.count(firstNumber, secondNumber);
    }
    return result;
  }
};
