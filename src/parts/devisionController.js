export default {
  setFirst(arg) {
    return arg;
  },
  setSecond(arg) {
    return arg;
  },
  count(firstNumber, secondNumber) {
    return firstNumber / secondNumber;
  },
};
