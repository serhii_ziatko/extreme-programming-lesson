export default {
  setFirst(arg) {
    return arg;
  },
  setSecond(arg) {
    return arg;
  },
  setAction(arg) {
    return arg;
  },
  count(first, second) {
    return first * second;
  }
};
